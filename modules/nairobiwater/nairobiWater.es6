class NairobiWater extends JambopayGeneric
{

	constructor (  )
	{
		super("nairobiwater");
	}


	//# Expects { AccountNumber }
    get_bill ( params, headers = {} )
    {


        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetBill','GET', params , headers));

        });

    };

}

module.exports = NairobiWater;