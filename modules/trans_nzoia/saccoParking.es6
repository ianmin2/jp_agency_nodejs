class SaccoParking extends JambopayGeneric
{

	constructor (  )
	{
        super( "saccoparking" );
        this.MerchantID = "TRANS";
	}

    get_saccos ( headers = {} )
    {

        let params = {
            Stream : this.Stream,
            MerchantID : this.MerchantID
        };

        return new Promise( function(resolve,reject)
        {
                        
            resolve(remote('/api/SaccoParking/GetSaccos','GET', params, headers));

        });


    } 


	//# Expects { }
    get_sacco_parking_details ( headers = {} )
    {

        let params = {
            Stream : this.Stream,
            MerchantID : this.MerchantID
        };

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetSaccoParkingDetails','GET', params, headers));

        });

    };

    prepare_payment ( BillData = {} , headers = {} )
    {
     
     
         BillData        = json( BillData );      
             
         BillData.Stream = ( BillData.Stream != undefined ) ? BillData.Stream : ( this.Stream != undefined ) ? this.Stream : "merchantinvoice";
         
         BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;
         
         BillData.MerchantID     = "TRANS";
 
        return new Promise( function(resolve,reject)
        {
 
            resolve(remote('/api/payments/Post','POST', BillData, headers));
 
        });
 
    };
 
 
     commit_payment ( BillData, headers = {} )
     {
         
         
         BillData                = BillData || {   };
         BillData.Stream         = this.Stream ;
         BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;
         BillData.MerchantID     = "TRANS";

         return new Promise( function(resolve,reject)
         {
             
             resolve(remote('/api/payments/Put','PUT', BillData, headers));
 
         });
 
     };
 

}

module.exports = SaccoParking