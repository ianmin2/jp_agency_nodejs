class Reports
{

	constructor(  stream )
	{
	
		if( stream && stream.length > 0 )
		{		
			this.stream = stream;		
		}
		else 
		{		
			throw new Error(`Please define a valid reporting stream.\n\n\te.g\tconst parking_reports = new JP.reports('parking');`);
		}

	}

	//# Expects { PlateNumber,Index,TransactionID,StartDate,EndDate,UserID,TransactionStatus }
    get_transactions ( params = {} , headers = {} )
    {
	
        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetTransactions','GET', params , headers));

        });

    };

}

module.exports = Reports;