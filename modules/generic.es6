class Generic 
{

    constructor ( Stream )
    {
        this.Stream = Stream || undefined;
    }

    //# Expects {username,password,grant_type}
    login ( credentials )
    {
        return new Promise( function(resolve,reject)
        {

            remote( '/token', 'POST', credentials )
            .then( login_data => {

                //@ Make the JAMBOPAY_TOKEN globally accessible
                JAMBOPAY_TOKEN          = `Bearer ${JSON.parse( login_data).access_token }`;

                //@ Store the login credentials so as to facilitate token refresh
                JAMBOPAY_NEXT_LOGIN.credentials = credentials;

                //@ Clear the already existing token refresh interval
                clearInterval( JAMBOPAY_NEXT_LOGIN.timer );
                
                //@ Calculate the time before token expiry and subtract three hours
                let next_token_interval = ( new Date(JSON.parse(login_data)[`.expires`]) - new Date(JSON.parse(login_data)[`.issued`]) ) - 10800000;

                //@ Set an asyncronous automated token refresh command
                JAMBOPAY_NEXT_LOGIN.timer = setInterval(()=>{

                    //@ Attempt a login
                    JP.login( JAMBOPAY_NEXT_LOGIN.credentials )
                    //@ Catch Faiilures
                    .catch(e => {

                        //@ Upon failure, clear the interval
                        clearInterval( JAMBOPAY_NEXT_LOGIN.timer );

                        //@ Try every ten minutes 
                        JAMBOPAY_NEXT_LOGIN.timer = setInterval( () => {

                            JP.login( JAMBOPAY_NEXT_LOGIN.credentials )
                            .catch(e=>{console.log(e.message);})
                            
                        },600000);

                    })

                },next_token_interval);


                resolve( login_data );

            })
            .catch( fail_data => {

                reject( fail_data );

            })

            

        })
    };
    
    //# Expects { endpointURL as string }
    set_endpoint_url ( endpointUrl="http://test.jambopay.co.ke/jambopayservices"  )
    {
        return new Promise( function(resolve,reject){

            endpointUrl = ( endpointUrl == "http://test.jambopay.co.ke/jambopayservices" ) 
                            ? endpointUrl 
                            : `${endpointUrl.replace(/\s|\/$/ig,"").replace(/\/jambopayservices/ig,'')}/jambopayservices`;
            Object.assign(global, { JAMBOPAY: endpointUrl });
            resolve(`The endpoint url has successfuly been set to ${ endpointUrl.replace(/\/jambopayservices/ig,'')}.`);

        })
    
    };

    //# Expects { RevenueStreamID, MerchantID, Narration,  PhoneNumber, Amount, stream="merchantbill" }
    bill ( BillData, headers = {} )
    {
        BillData            = BillData || {   };
        BillData.Stream     = BillData.Stream || BillData.stream || this.Stream || "merchantbill";
        
        return new Promise( function(resolve,reject)
        {

            resolve(remote('/api/payments/Post','POST', BillData, headers));

        });

    };

    //# Expects {  BillNumber, Year, Month, stream="merchantbill", PhoneNumber}
    bill_status ( BillData, headers = {} )
    {
        
        BillData            = BillData || {   };
        BillData.Stream     = BillData.Stream  || this.Stream || "merchantbill";
        
        return new Promise( function(resolve,reject)
        {

            resolve(remote('/api/payments/GetBill','GET', BillData, headers));

        });

    };

        
    //# Expects {  MerchantID, PhoneNumber, BillNumber, PaymentTypeID=1, stream="merchantinvoice", PhoneNumber}
    get_bill ( BillData, headers = {} )
    {
       
        BillData                = BillData || {   };
        BillData.Stream         = BillData.Stream  || this.Stream || "merchantinvoice";
        BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;
         
        return new Promise( function(resolve,reject)
        {

            resolve(remote('/api/payments/Post','POST', BillData, headers));

        });

    };

   
   prepare_payment ( BillData = {} , headers = {} )
   {
    
    
        BillData        = json( BillData );      
            
        BillData.Stream = ( BillData.Stream != undefined ) ? BillData.Stream : ( this.Stream != undefined ) ? this.Stream : "merchantinvoice";
        
        BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;

       return new Promise( function(resolve,reject)
       {

           resolve(remote('/api/payments/Post','POST', BillData, headers));

       });

   };

    //# Expects {  TransactionID, Tendered, PaidBy, stream="merchantinvoice" }
    pay_bill ( BillData, headers = {} )
    {
        
        BillData                = json(BillData) || {   };
        BillData.Stream         = BillData.Stream  || this.Stream || "merchantinvoice";
        BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;
        
        return new Promise( function(resolve,reject)
        {

            resolve(remote('/api/payments/Put','PUT', BillData, headers));

        });

    };

    commit_payment ( BillData, headers = {} )
    {
        
        
        BillData                = BillData || {   };
        BillData.Stream         = BillData.Stream  || this.Stream || "merchantinvoice";
        BillData.PaymentTypeID  = BillData.PaymentTypeID || 1;


        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/Put','PUT', BillData, headers));

        });

    };

    //# Expects { billNumber, stream="merchantbill" }
    get_merchant_streams (merchantID, Stream="merchantbill", headers = {} )
    {

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetMerchantStreams','GET', {merchantID,Stream}, headers));

        });

    };

}

module.exports = Generic;
Object.assign( global, { JambopayGeneric : Generic } );
Object.assign( global, { JP_GENERIC : new  Generic() } )
module.exports = JP_GENERIC;