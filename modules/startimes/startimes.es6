class Startimes extends JambopayGeneric
{

    constructor () 
    {
        super( "startimes" );
    }

    //# Expects {  { SmartCardCode, CustomerCode } , stream="startimes" }
    get_transactions ( data, headers = {} )
    {
	
        let Stream = this.Stream;
        let d = { SmartCardCode: d.SmartCardCode, CustomerCode: d.SmartCardCode, Stream : this.stream  };
            

        return new Promise( function(resolve,reject)
        {
            console.log(`Parameters Passed ==>\n`)
            console.dir(d);
            console.log(`________________________________________\n\n`)
            resolve(remote('/api/payments/GetTransactions','GET', d, headers));

        });

    };

}

module.exports = Startimes;