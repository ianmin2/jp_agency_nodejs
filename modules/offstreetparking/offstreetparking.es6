class OffStreetParking extends JambopayGeneric
{

    constructor( isLive = false )
    {
        this.endpointURL = ( isLive ) ? `http://192.168.11.167` : `http://cashoffice.nairobi.go.ke/Offstreet`
        super("offstreetparking");
    }

    //@ Expects {PhoneNumber}, headers
    validate_attendant ( loginData, headers = {} )
    {

        return new Promise(  function(resolve,reject)
        {
            
            resolve(custom_remote(`${this.endpointURL}/api/Login`,'GET', loginData, headers));

        });

    };

    //@ Expects {PhoneNumber,attPhoneNumber} , headers
    book_vehicle ( vehicleData, headers = {} )
    {

        return new Promise(  function(resolve,reject)
        {
            
            resolve(custom_remote(`${this.endpointURL}/api/Booking`,'GET', vehicleData, headers));

        });

    };

    //@ Expects {plateNumber} , headers
    get_booking_info ( vehicleData, headers = {} )
    {

        return new Promise(  function(resolve,reject)
        {
            
            resolve(custom_remote(`${this.endpointURL}/api/Parking`,'GET', vehicleData, headers));

        });

    };

    //@ Expects {PlateNumber,PaidBy,ReceiptNumber,TranID} , headers
    payment_notification ( vehicleData, headers = {} )
    {

        return new Promise(  function(resolve,reject)
        {

            resolve(custom_remote(`${this.endpointURL}/Offstreet/api/Status`,'POST', vehicleData, headers));

        });

    };

}

module.exports = OffStreetParking;