class Kiwasco extends JambopayGeneric {

    constructor ( params =  "kisumuwaterinvoice"  )
    {
        super(params);
    }

    //# Expects { accountNumber, stream="kisumuwaterinvoice" }
    get_account_details (accountNumber, headers = {} )
    {
	
        let Stream = this.Stream;
        let d = {accountNumber,Stream,AccountNumber: accountNumber};
            

        return new Promise( function(resolve,reject)
        {
            console.log(`Parameters Passed ==>\n`)
            console.dir(d);
            console.log(`________________________________________\n\n`)
            resolve(remote('/api/payments/GetAccountDetails','GET', d, headers));

        });

    };
  
}

module.exports = Kiwasco;