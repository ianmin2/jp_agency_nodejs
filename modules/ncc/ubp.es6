class UBP extends JambopayGeneric {

    constructor () {
        super("sbp");
    }

    //@Expects {PhoneNumber,ActivityID}
    get_sbp_classes(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/getsbpclasses','GET', params , headers));

        });  

    };

     //@Expects {PhoneNumber,ID}
     get_sbp_sub_classes(  params = {}, headers = {} )
     {
 
         params.Stream = this.Stream;
 
         return new Promise( function(resolve,reject)
         {
             
             resolve(remote('/api/payments/getsbpsubclasses','GET', params , headers));
 
         });  
 
     };

    //@Expects {PhoneNumber,subclassID}
    get_sbp_activity_charges(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetSBPActivityCharges','GET', params , headers));

        });  

    };

    //@Expects {}
    get_sbp_activity_sub_counties( headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/getsbpsubcounties','GET', params , headers));

        });  

    };

    //@Expects {id}
    get_sbp_wards(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/getsbpwards','GET', params , headers));

        });  

    };

    //@Expects {LicenseID,Year,PhoneNumber}
    get_business(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetBusiness','GET', params , headers));

        });  

    };

    //@Expects {ActivityID,PhoneNumber}
    get_business_class_id(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetSBPClasses','GET', params , headers));

        });  

    };

    //@Expects {ID,PhoneNumber}
    get_business_sub_class_id(  params = {}, headers = {} )
    {

        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/getsbpsubclasses','GET', params , headers));

        });  

    };


}

module.exports = UBP;