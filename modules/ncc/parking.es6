class Parking extends JambopayGeneric {

	constructor () 
	{	
		super("parking");
	}

	//# Expects { }
    get_daily_parking_items ( headers = {} )
    {

        let Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetDailyParkingItems','GET', {Stream}, headers));

        });

    };

	//# Expects { }
    get_seasonal_parking_items ( headers = {} )
    {

        let Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetSeasonalParkingItems','GET', {Stream}, headers));

        });

    };

}

module.exports = Parking;