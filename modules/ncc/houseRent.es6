class HouseRent extends JambopayGeneric
{

	constructor (  )
	{
		super("rent");
	}


	//# Expects { }
    get_estates ( headers = {} )
    {


        let Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetEstates','GET', {Stream}, headers));

        });

    };


	//# Expects { ID }
    get_residences ( params, headers = {} )
    {


        params.Stream = this.Stream;

        return new Promise( function(resolve,reject)
        {
            
            resolve(remote('/api/payments/GetResidences','GET', params , headers));

        });

    };

}

module.exports = HouseRent;