/* 
 * NAIROBI DEPRECATED AFTER END OF CONTRACT 

	//@ NAIROBI COUNTY COUNCIL
	const EConstruction = require( path.join(__dirname, '/ncc/eConstruction.es6' ) );
	const HouseRent 	= require( path.join(__dirname, '/ncc/houseRent.es6' ) );
	const LandRates     = require( path.join(__dirname, '/ncc/landRates.es6' ) );
	const Liquor 		= require( path.join(__dirname, '/ncc/liquor.es6' ) );
	const Miscellaneous = require( path.join(__dirname, '/ncc/miscellaneous.es6' ) );
	const Parking   	= require( path.join(__dirname, '/ncc/parking.es6' ) );
	const SaccoParking  = require( path.join(__dirname, '/ncc/saccoParking.es6' ) );
	const UBP 			= require( path.join(__dirname, '/ncc/ubp.es6' ) );

	//@ NCC- Offstreet Parking
	const OffstreetParking = require( path.join( __dirname, '/offstreetparking/offstreetparking.es6') );

*/

//@ NAIROBI WATER & SEWERAGE COMPANY
const NairobiWater 	= require( path.join( __dirname, '/nairobiwater/nairobiWater.es6' ) );

//@ KASNEB
const Kasneb 		= require( path.join( __dirname, '/kasneb/kasneb.es6' ) );

//@ KISUMU WATER & SEWERAGE COMPANY
const Kiwasco 		= require( path.join( __dirname, '/kiwasco/kiwasco.es6' ) );

//@ KENYA POWER & LIGHTING COMPANY
const KPLC 			= require( path.join( __dirname , 'kplc/kplc.es6') );

//@ NATIONAL HOSPITAL INSURANCE FUND
const NHIF 			= require( path.join( __dirname, '/nhif/nhif.es6' ) );

//@ REPORTS
const Reports 		= require( path.join( __dirname, '/reports/reports.es6' ) );

//@ STARTIMES  
const Startimes 	= require( path.join( __dirname, '/startimes/startimes.es6' ) );

//@ Wallet Related Actions
const Wallet 		= require( path.join( __dirname, '/wallet/wallet.es6') );

/* 
 * DEPRECATED DUE TO MIGRATION TO COUNTY SYSTEM V2

	//@ Trans Nzoia County
	const Trans_Sacco_Parking 	= require( path.join( __dirname, '/trans_nzoia/saccoParking.es6' ) );

*/

module. exports = {

	kasneb 			: new Kasneb(),

	kiwasco 		: new Kiwasco(),

	kplc			: new KPLC(),
	
	/*
	* DEPRECATED AFTER CONTRACT EXPIRY
		ncc				: 
						{
							e_construction	: new EConstruction(),
							house_rent		: new HouseRent(),
							land_rates 		: new LandRates(),
							liquor			: new Liquor(),
							misc         	: new Miscellaneous(),
							parking			: new Parking(),						
							sacco_parking	: new SaccoParking(),
							OffStreetParking: OffstreetParking,
							ubp				: new UBP()		
						},
	*/

	/*
	 * FUNCTION MIGRATED TO COUNTY V2
		tnc				: 
						{
							sacco_parking	: new Trans_Sacco_Parking()
						},
	*/

	nairobi_water	: new NairobiWater(),

	nhif			: new NHIF(),

	Reports,

	startimes 		: new Startimes(),

	media 			: 
	{
		startimes   : new Startimes(),
	},

	water 			: 
	{
		nairobi 	: new NairobiWater(),
		kisumu      : new Kiwasco(),
	},

	wallet 			: new Wallet(),

};