class Airtime extends JambopayGeneric
{

    constructor () 
    {
        super( "credit" );
    }

    get_providers()
    {
        let Stream = "creditapi";
        let d = {}

        return new Promise(function(){
            resolve(remote(`/api/credit/getcredentials`,'GET', d, headers));
        });    
    }

    // @override
    // prepare_payment('Operator ID', 'Recipient Phone Number', 'Topup Amount')
    prepare_payment( operator, recipient, amount, PaidBy = 'SDK')
    {
        if(operator&& recipient&&amount&&PaidBy)
        {
            let d =
            {
                Denomination        : 0,
                PaidBy              : PaidBy,
                ProductCode         : "RV",
                Stream              : this.Stream,
                CircleCode          : "*",
                PaymentTypeID       : 1,
                Quantity            : 1,
                OperatorCode        : operator,
                PhoneNumber         : recipient,
                Amount              : amount,
                password            : JAMBOPAY_NEXT_LOGIN.credentials.password
            };

            return new Promise(function(resolve,reject){
                resolve(remote(`/api/payments/post`,`POST`,d,headers));
            });
        }
        else
        {
            reject(new Error(`The 'prepare_payment' method expects parameters.\nPlease call it in the following format\n prepare_payment('Operator ID', 'Recipient Phone Number', 'Topup Amount')`));
        }

    }


    //@ override
    commit_payment( txInfo )
    {

        if(typeof txInfo == 'object')
        {
            
            txInfo.Denomination     = 0;
            txInfo.ProductCode      = 'RV';
            txInfo.Stream           = this.Stream;

            return new Promise(function(resolve,reject){
                resolve(remote(`/api/payments/put`,`PUT`,txInfo,headers));
            });

        }
        else
        {
            reject(new Error(`The 'commit_payment' method expects the resultant of the 'prepare_payment' method.`));
        }        

    }


}

module.exports = Airtime;