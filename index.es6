/*
    For connection to the Jambopay API
*/

//@ Allow the setting of endpoints with self signed certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//@ Import the configuration file
require( "./config.es6" );


//@ Include the generic Jambopay services handler
require( path.join(__dirname,`/modules/generic.es6`) );

//@ Assign the generic methods to the module
Object.assign( exports, { 
                            login               : JP_GENERIC.login,  
                            set_endpoint_url    : JP_GENERIC.set_endpoint_url,
                            get_bill            : JP_GENERIC.get_bill,
                            pay_bill            : JP_GENERIC.pay_bill
                        });

//@ Import the main endpoint configuration file
Object.assign( exports, require( path.join( __dirname, `/modules/index.es6` ) ) );


// //@ Avail the .login function on the importation of this module
// exports.login = ( credentials ) => 
// {
//     return new Login( credentials);
// };


// // @ Avail the .set_endpoint method
// exports.set_endpoint_url = ( endpointUrl ) =>
// {
//     return new SetEndpointURL( endpointUrl );
// }


// //@ Avail the .bill function on the importation of this module
// exports.bill = (BillData, headers ) => 
// {
//     return new MakeBill( BillData, headers);
// };



// //@ Avail the .bill function on the importation of this module
// exports.bill_status = (BillData, headers ) => 
// {
//     return new BillStatus( BillData, headers);
// };


// //@ Avail the .bill function on the importation of this module
// exports.get_bill = (BillData, headers ) => 
// {
//     return new GetBill( BillData, headers);
// };


// //@ Avail the .bill function on the importation of this module
// exports.pay_bill = (BillData, headers ) => 
// {
//     return new PayBill( BillData, headers);
// };



// //@ Avail the .get_merchant_streams function on the importation of this module
// exports.get_merchant_streams = ( merchantID, Stream="merchantbill", headers ) => 
// {
//     return new GetMerchantStreams( merchantID, Stream, headers);
// };

/*
//# Expects { stream="saccoparking" }
function GetSaccoParkingDetails (Stream="saccoparking", headers )
{

    return new Promise( function(resolve,reject)
    {
        
        resolve(remote('/api/payments/GetSaccoParkingDetails','GET', {Stream}, headers));

    });

};
//@ Avail the .get_sacco_parking_details function on the importation of this module
exports.get_sacco_parking_details = ( Stream="saccoparking", headers ) => 
{
    return new GetSaccoParkingDetails( Stream, headers);
};

//# Expects { Stream="parking", MerchantID = "NCC"  }
function GetDailyParkingItems ( Stream = "parking" , headers )
{

    return new Promise( function(resolve,reject)
    {
        
        resolve(remote('/api/payments/GetDailyParkingItems','GET', { Stream : "parking" }, headers));

    });

};
//@ Avail the .get_sacco_parking_details function on the importation of this module
exports.get_daily_parking_items = ( Stream = "parking" , headers ) => 
{
    return new GetDailyParkingItems( Stream, headers);
};
*/

/* WALLET 
//# Expects { PhoneNumber, Amount, PaymentTypeID=1, stream="wallet" }
function PrepareTopup ( params={}, headers )
{

    params.PaymentTypeID    = PaymentTypeID || 1;
    params.Stream           = params.Stream || 'wallet';

    return new Promise( function(resolve,reject)
    {

        resolve(remote('/api/payments/POST','POST',params, headers));

    });

};
//@ Avail the .start_topup function
exports.start_topup  = ( params, headers ) => 
{
    return new PrepareTopup(params,headers);
};

//# Expects { TransactionID, PhoneNumber, Stream="wallet" }
function CommitTopup ( params={}, headers )
{

    params.Stream           = params.Stream || 'wallet';

    return new Promise( function(response,resolve) 
    {
        resolve(remote('/api/payments/PUT','PUT',params,headers));
    })

};
//@ Avail the .confirm_topup method
exports.confirm_topup   = ( params, headers ) =>
{
    return new CommitTopup(params,headers);
}
*/
// exports.kiwasco = require(path.join(__dirname,'/modules/kiwasco/kiwasco.es6'))