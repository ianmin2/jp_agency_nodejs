//@ Globally accessible methods for data conversion

Object.assign( global, {
    json    : ( obj ) => ( typeof(obj) === 'object' ) ? obj : JSON.parse( obj ),
    str     : ( obj ) => ( typeof(obj) === "object" ) ? JSON.stringify(obj) : obj,
    clone   : (obj) => JSON.parse( JSON.stringify( obj ) )
});

//@ DEFINE THE ACCESS URL

    //@ Attach the jambopay main endpoint url to the global scope as "JAMBOPAY"
    Object.assign(global, { JAMBOPAY: 'http://192.168.11.22/jambopayservices' });    //  ["https://services.jambopay.co.ke/JamboPayServices" for production]
 
    //@ Define the jambopay_token parameter
    Object.assign(global, { JAMBOPAY_TOKEN : "" });

    //@ Define the Jambopay Application Key Parameter
    Object.assign(global, { APP_KEY : "" });

    //@ Attach the next login timer to the global scope as "JAMBOPAY_NEXT_LOGIN_TIMER"
    Object.assign(global, { JAMBOPAY_NEXT_LOGIN : { credentials: {}, timer: "" } });

//@ PERFORM THE IMPORTS

    //@ Attach the native path module to the global scope as "path"
    Object.assign(global, {path: require('path')});

    //@ Attach the native fs module to the global scope as "fs"
    Object.assign(global, require("fs"))

    //@ Attach the request-promise http module to the global scope as "request"
    Object.assign(global, {request: require("request-promise")});

    //@ Attach the querystring http module to the global scope as "qs"
    Object.assign(global, {qs: require("query-string")});
  
    //@ Attach the response formatter to the global scope as "make_response"
    Object.assign(global, {make_response: ( response=200, message="Sample Response", command="done") => 
        {
            return {
                response
                ,data : { message,command}
            }
        }
    });
    
    Object.assign(global, {
        remote : ( target, method, body, headers = {'content-type': 'application/x-www-form-urlencoded' } ) =>
        {

            //@ Convert the http verb to uppercase for easy evaluation
            method = method.toUpperCase();

            switch (method) 
            {
                case "POST":
                case "PUT":
                case "DELETE":
                
                    //@ Ensure that the required headers are set
                    headers["content-type"]     = 'application/x-www-form-urlencoded';
                    headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : JAMBOPAY_TOKEN;
                    headers["app_key"]          = ( headers["app_key"] ) ? headers["app_key"] : APP_KEY;
                    headers["accept"]           = "application/json";

                    //@ Capture the constituent keys of the body object
                    let kys = Object.keys(body)

                    //@ Filter to check for any objects that might be arrays
                    let arrs = kys.filter(ky => Array.isArray( body[ky] ));

                    //@ Remove any null - valued arrays
                    arrs = arrs.filter(k => (body[k] != undefined && body[k] != null && body[k] != ""));

                    //@ Loop through each 'surviving' refrence key in the array object 
                    arrs.forEach( (a) => 
                    {  

                        //@ loop through for as many keys as are in the array
                        body[a].forEach( (value_at_body_a,body_a_index) => 
                        {
                            
                            //@ Capture the Object keys at the relevant index
                            let obj_kys = Object.keys(body[a][body_a_index]);

                            //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                            if( obj_kys[0] != 0 )
                            {

                                //@ Loop through each available object key
                                obj_kys.forEach( obj_ky => 
                                {
    
                                    // console.log( `${obj_ky} => ${body[a][0][obj_ky]}` )
                                    //@ Populate the body object with the given parameter in the desired format
                                    body[`${a}[${body_a_index}].${obj_ky}`] = body[a][body_a_index][obj_ky];
    
                                });    
    
                            }

                        });

                        // console.log(obj_kys)
                        delete body[a];
                       
                    });

                    /*
                    // console.dir(arrs)
                    console.log(`\n\n==========================================================\n`)
                    console.dir(body)
                    console.log(`\n\n==========================================================\n`)

                    // return Promise.resolve( arrs );
                    */

                    let reque = qs.stringify(body,{arrayFormat: 'index'})
                    // let reque = body;

                    // console.dir( body );



                    return  request( {
                        uri : `${JAMBOPAY}${target}`,
                        method: method,
                        body: reque,
                        headers: headers
                    });

                break;

                case "GET":

                    //@ Ensure that the required headers are set
                    headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : JAMBOPAY_TOKEN;
                    headers["app_key"]          = ( headers["app_key"] ) ? headers["app_key"] : APP_KEY;
                    headers["accept"]           = "application/json";

                    return  request( {
                        uri : `${JAMBOPAY}${target}`,
                        qs: (body),
                        headers: headers
                    });

                break;
            
                default:
                    return Promise.reject({ message : "An invalid http verb was specified for use with Jambopay."  });
                break;
            }           
            
        },
        custom_remote : ( target, method, body, headers = {'content-type': 'application/x-www-form-urlencoded' } ) =>
        {

            //@ Convert the http verb to uppercase for easy evaluation
            method = method.toUpperCase();

            switch (method) 
            {
                case "POST":
                case "PUT":
                case "DELETE":
                
                    //@ Ensure that headers are set
                    headers["content-type"] = 'application/x-www-form-urlencoded';
                    headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : JAMBOPAY_TOKEN;
                    headers["app_key"]          = ( headers["app_key"] ) ? headers["app_key"] : APP_KEY;

                    //@ Capture the constituent keys of the body object
                    let kys = Object.keys(body)

                    //@ Filter to check for any objects that might be arrays
                    let arrs = kys.filter(ky => Array.isArray( body[ky] ));

                    //@ Remove any null - valued arrays
                    arrs = arrs.filter(k => (body[k] != undefined && body[k] != null && body[k] != ""));

                    //@ Loop through each 'surviving' refrence key in the array object 
                    arrs.forEach( (a) => 
                    {  

                        //@ loop through for as many keys as are in the array
                        body[a].forEach( (value_at_body_a,body_a_index) => 
                        {
                            
                            //@ Capture the Object keys at the relevant index
                            let obj_kys = Object.keys(body[a][body_a_index]);

                            //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                            if( obj_kys[0] != 0 )
                            {

                                //@ Loop through each available object key
                                obj_kys.forEach( obj_ky => 
                                {
    
                                    // console.log( `${obj_ky} => ${body[a][0][obj_ky]}` )
                                    //@ Populate the body object with the given parameter in the desired format
                                    body[`${a}[${body_a_index}].${obj_ky}`] = body[a][body_a_index][obj_ky];
    
                                });    
    
                            }

                        });

                        // console.log(obj_kys)
                       
                    });

                    // console.dir(arrs)

                    // console.dir(body)

                    // return Promise.resolve( arrs );

                    let reque = qs.stringify(body,{arrayFormat: 'index'})
                    // let reque = body;

                    // console.log( `Sending the request:\n ${(reque)}` );

                    return  request( {
                        uri : `${target}`,
                        method: method,
                        body: reque,
                        headers: headers
                    });

                break;

                case "GET":
                
                //@ Ensure that the required headers are set
                headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : JAMBOPAY_TOKEN;
                headers["app_key"]          = ( headers["app_key"] ) ? headers["app_key"] : APP_KEY;

                    return  request( {
                        uri : `${target}`,
                        qs: (body),
                        headers: headers
                    });
                break;
            
                default:
                    return Promise.reject({ message : "An invalid http verb was specified for use with Jambopay."  });
                break;
            }           
            
        } 
    });