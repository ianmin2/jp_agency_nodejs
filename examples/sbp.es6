const vars = {
    "BusinessID": 1312386,
    "Year": 2019,
    "Names": 'Someone',
    "AnnualAmount": 209200,
    "BillNumber": "BP18-000083",
    "Building": 34634,
    "BillStatus": "NEW",
    "BusinessName": "SMART DRIVE LIMITED",
    "Floor": 3,
    "PhysicalAddress": "MASARI RD",
    "RoomStallNumber": 12,
    "RegistrationFee": 0,
    "Penalty": 0,
    "Pin": null,
    "Commission": 0,
    "ActivityCode": 105,
    "ApprovalStatus": 0,
    "DateIssued": "0001-01-01T00:00:00",
    "SBPPaymentType": 0,
    "PlotNumber": 112,
    "BusinessRegistrationNumber": 112,
    "VAT": null,
    "IDDocumentNumber": null,
    "IDNumber": 0,
    "ZoneCode": 0,
    "WardCode": 0,
    "BusinessClassificationDetails": null,
    "OtherClassificationDetails": null,
    "AnnualSBPAmount": 0,
    "POBox": null,
    "PostalCode": null,
    "PremisesArea": 121,
    "NumberOfEmployees": 0,
    "RelativeSize": 0,
    "Town": null,
    "Telephone1": null,
    "Telephone2": null,
    "FaxNumber": null,
    "Email": null,
    "ContactPersonDesignation": null,
    "ContactPersonPOBox": null,
    "ContactPersonPostalCode": null,
    "ContactPersonTown": null,
    "ContactPersonTelephone1": null,
    "ContactPersonName": null,
    "ContactPersonTelephone2": null,
    "ContactPersonFaxNumber": null,
    "PhonePin": null,
    "ActivityName": "Large trader shop or retail service: 21- 50 employees / 300 - 3000 sq.m / prime location",
    "IDNumberName": null,
    "RelativeSizeName": null,
    "WardName": null,
    "ZoneName": "",
    "AcceptedTnC": true,
    "BuildingCategory": 'Non-Storey',
    "HasKitchen": false,
    "Exclusions": null,
    "LineItems": [
        {
            "ID": 0,
            "Name": "tradeLicensefee",
            "Description": "Trade License",
            "Status": 0,
            "Amount": 120000,
            "Narration": null,
            "PlotNumber": 112
        },
        {
            "ID": 0,
            "Name": "FirePermitLicenseFee",
            "Description": "Fire Permit License",
            "Status": 0,
            "Amount": 25000,
            "Narration": null,
            "PlotNumber": 112
        },
        {
            "ID": 0,
            "Name": "healthCertificate",
            "Description": "Health Certificate",
            "Status": 0,
            "Amount": 10000,
            "Narration": null,
            "PlotNumber": 112
        },
        {
            "ID": 0,
            "Name": "FoodHygiene",
            "Description": "Food Hygiene Certificate",
            "Status": 0,
            "Amount": 50000,
            "Narration": null,
            "PlotNumber": 112
        },
        {
            "ID": 0,
            "Name": "Advertisement",
            "Description": "Advertisement",
            "Status": 0,
            "Amount": 4200,
            "Narration": null,
            "PlotNumber": 112
        }
    ],
    "HasAdvertizement": false,
    "BusinessStatus": 0,
    "ExcludeFire": false,
    "ExcludeHealth": false,
    "ExcludeFood": false,
    "ExcludeAdvertizement": false,
    "SBPSubClassID": 1382308,
    "ID": "dc3dcd23-cd10-e811-944a-7427ea2f7f59",
    "ReceiptNumber": null,
    "LocalReceiptNumber": null,
    "TransactionDate": null,
    "ReferenceNumber": null,
    "BankName": null,
    "BranchName": null,
    "Payee": null,
    "PaymentTypeID": 1,
    "Amount": 209200,
    "PaidBy": null,
    "Tendered": 0,
    "SourceIP": null,
    "TransactionID": "1008429",
    "MerchantID": "NCC",
    "Narration": null,
    "PaymentDetails": null,
    "PhoneNumber": "254712999267"
}


JP = require(`../index.es6`);

// JP.set_endpoint_url("http://localhost:1234")
JP.set_endpoint_url()
.then( a => {

    // console.log(`\n\n ${JAMBOPAY} \n\n`)

    // console.log(a);

    // console.log(`\n\n\nENDPOINT SET SUCCESSFULY.\n\n\n`)

    JP.login({ 

        username: "teller1.branch1@agency.com",

        password: "p@ssw0rd",

        grant_type: "agency"

    })
    .then(d => {


        var pars = {  "LicenceID": 1312386, "Year": 2018, "PhoneNumber": "0725678447" };

        JP
        .ncc
        .ubp
        .get_business( pars,{
            "authorization" :JAMBOPAY_TOKEN,
            "app_key"       :"BBD14E65-5B05-E611-9411-7427EA2F7F59"
        })
        .then(json)
        .then(d => {
            
            console.dir(d)

            
                    // PlotNumber: 112,
                    // Floor       : 1,
                    // RoomStallNumber: 112


            // JP
            // .ncc
            // .ubp
            // .commit_payment( 
            //     {
            //         PhoneNumber : d.PhoneNumber ,  //|| '0725678447'
            //         SBPPaymentType: d.SBPPaymentType , //|| 1
            //         Tendered    : d.Amount, //  || 1000
            //         TransactionID: d.TransactionID,
            //         PaymentTypeID: 1,
            //         PaidBy      : "DTB",
            //         PhonePin    : "1234", 
            //         PlotNumber: 112,
            //         Floor       : 1,
            //         RoomStallNumber: 112,
            //         "Building": 34634,
            //         "WardCode": 22,
            //     },{
            //     "authorization" :JAMBOPAY_TOKEN,
            //     "app_key"       :"BBD14E65-5B05-E611-9411-7427EA2F7F59"
            // })
            // .then(json)
            // .then(console.dir)
            // .catch(e=>console.log(e.message))


        })
        .catch(e=>console.log(e.message))

    
    })

})